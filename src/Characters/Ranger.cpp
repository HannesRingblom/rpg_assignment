#include "Ranger.hpp"

Ranger::Ranger(int body, int speed, int knowledge, int level ):Character(level){

    this->setBody(body);
    this->setSpeed(speed);
    this->setKnowledge(knowledge); 
    this->setRangedDamage(10);
}

void Ranger::levelUp(int new_level){

    this->set_level(new_level);
    this->setBody(new_level * 5);
    this->setSpeed(new_level * 5);
    this->setKnowledge(new_level * 2);
}

void Ranger::setRangedDamage(int dmg){
    this->setRangedDamage(dmg);
}

int Ranger::attack(){
    return 0;
}

int Ranger::getDamage(){

    int dmg;
    dmg += this->getRangedDamage();

}