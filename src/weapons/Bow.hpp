#pragma once
#include "Weapon.hpp"

class Bow : public Weapon
{
  private:
    int ranged_damage;

  public:

	Bow(int power ):Weapon(power){};
    void upgrade();
    int getTotalPower();
    int getRangedDamage();
};
