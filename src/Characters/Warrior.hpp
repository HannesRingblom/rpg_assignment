#pragma once
#include "Character.hpp"

class Warrior : public Character
{
  public:

	Warrior(int body, int speed, int knowledge, int level ):Character(level){};
    void levelUp(int new_level);

    void set_level(int level);
    void equipWeapon(Sword sword);
    int attack();

};

