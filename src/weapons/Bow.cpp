#include "Bow.hpp"

Bow::Bow(int start_power):Weapon(start_power){}

void Bow::upgrade(){
    this->ranged_damage += 5;
}
int Bow::getRangedDamage(){
    return this->ranged_damage;
}
int Bow::getTotalPower(){
    int power;
    power += this->ranged_damage + this->getPower();
    return power;
}