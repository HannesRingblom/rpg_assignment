#include "Rogue.hpp"

Rogue::Rogue(int body, int speed, int knowledge, int level ):Character(level){

    this->setBody(body);
    this->setSpeed(speed);
    this->setKnowledge(knowledge); 
}

void Rogue::levelUp(int new_level){

    this->level = new_level;
    this->body= 10 * new_level;
    this->speed= 2 * new_level;
    this->knowledge= 1 * new_level;
}

int Rogue::attack(){
    return 0;
}