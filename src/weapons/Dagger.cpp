#include "Dagger.hpp"

Dagger::Dagger(int start_power):Weapon(start_power){}

void Dagger::upgrade(){
    this->melee_damage += 3;
    this->ranged_damage += 3;
}
int Dagger::getMeleeDamage(){
    return this->melee_damage;
}
int Dagger::getRangedDamage(){
    return this->ranged_damage;
}
int Dagger::getTotalPower(){
    int power;
    power += this->melee_damage + this->ranged_damage + this->getPower();
    return power;
}