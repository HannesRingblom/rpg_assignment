#pragma once
#include "Weapon.hpp"

class Staff : public Weapon
{
  private:
    int spell_power;

  public:

	  Staff(int power ):Weapon(power){};
    void upgrade();
    int getTotalPower();
    int getSpellPower();
};
