#include "Character.hpp"

Character::Character(int start_level)
  {
    this->level = start_level;
    this->hp = 100;
    this->body=0;
    this->speed=0;
    this->knowledge=0;
    this->melee_damage=0;
    this->ranged_damage=0;
    this->spell_power=0;
}

void Character::takeDamage(int damage){
  this->hp = this->hp - damage;
}
void Character::setSpeed(int new_speed){
  this->speed = new_speed;
}
void Character::setBody(int new_body){
  this->body = new_body;
}
void Character::setKnowledge(int new_knowledge){
  this->knowledge = new_knowledge;
}
void Character::attack(Character target, int damage){
  target.takeDamage(damage);
}