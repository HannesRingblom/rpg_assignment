#include "Weapon.hpp"

Weapon::Weapon(int start_power){

    this->power = start_power;
}

void Weapon::upgrade(int levels){
    this->power += 10*levels;
}