#include "Characters/Character.hpp"
#include "Characters/Warrior.hpp"
#include "Characters/Rogue.hpp"
#include "Characters/Mage.hpp"
#include "Characters/Ranger.hpp"

int main()
{
    Character warrior = Warrior(10,10,10,10);
    Character rogue = Rogue(10,10,10,10);
    Character mage = Mage(10,10,10,10);
    Character ranger = Ranger(10,10,10,10);
    std::cout<<warrior.getHp()<<std::endl;
    ranger.attack(warrior,ranger.getRangedDamage());
    std::cout<<warrior.getHp()<<std::endl;

    return 0;
}
