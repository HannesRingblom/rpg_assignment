#include "Warrior.hpp"

Warrior::Warrior(int body, int speed, int knowledge, int level ):Character(level){

    this->setBody(body);
    this->setSpeed(speed);
    this->setKnowledge(knowledge); 
}

void Warrior::levelUp(int new_level){

    this->set_level(new_level);
    this->setBody(new_level * 10);
    this->setSpeed(new_level * 2);
    this->setKnowledge(new_level);
}

int Warrior::attack(){

    return 0;
}