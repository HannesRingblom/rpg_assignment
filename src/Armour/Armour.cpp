#include "Armour.hpp"

Armour::Armour(int start_defence){

    this->defence = start_defence;
}

void Armour::upgrade(int levels){
    this->defence += 10*levels;
}

void Armour::setDefence(int new_defence){
    this->defence = new_defence;
}