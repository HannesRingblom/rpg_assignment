#include "Staff.hpp"

Staff::Staff(int start_power):Weapon(start_power){}

void Staff::upgrade(){
    this->spell_power += 5;
}
int Staff::getSpellPower(){
    return this->spell_power;
}
int Staff::getTotalPower(){
    int power;
    power += this->spell_power + this->getPower();
    return power;
}