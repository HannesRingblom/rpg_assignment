#pragma once
#include <iostream>
#include <list>

class Armour
{
private:
    int defence;

public:
    Armour(int power);
    ~Armour();

    //Accessors
    const int getDefence() const {return this-> defence;}

    //Setters
    void setDefence(int levels);

    //Functions
    void upgrade(int levels);
};