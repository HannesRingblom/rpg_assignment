#pragma once
#include "../weapons/Staff.hpp"
#include "../weapons/Dagger.hpp"
#include "../weapons/Bow.hpp"
#include "../weapons/Sword.hpp"
#include "../Armour/Armour.hpp"
#include <iostream>
#include <list>

class Character
{
private:
    
    int level;
    int hp;
    int body;
    int speed;
    int knowledge;

    int melee_damage;
    int ranged_damage;
    int spell_power;

public:
    Character(int level);
    ~Character();
    
    
    //Accessors
    const int& getLevel() const {return this-> level;}
    const int& getBody() const {return this-> body;}
    const int& getSpeed() const {return this-> speed;}
    const int& getKnowledge() const {return this-> knowledge;}
    const int& getMeleeDamage() const {return this-> melee_damage;}
    const int& getRangedDamage() const {return this-> ranged_damage;}
    const int& getSpellPower() const {return this-> spell_power;}
    const int& getHp() const {return this->hp;}

    //Setters
    void setSpeed(int speed);
    void setKnowledge(int knowledge);
    void setBody(int body);

    //Functions
    void takeDamage(int damage);
    void attack(Character target, int damage);
};

