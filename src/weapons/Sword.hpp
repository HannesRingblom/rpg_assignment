#pragma once
#include "Weapon.hpp"

class Sword : public Weapon
{
  private:
    int melee_damage;

  public:

	Sword(int power ):Weapon(power){};
    void upgrade();
    int getTotalPower();
    int getMeleeDamage();
};
