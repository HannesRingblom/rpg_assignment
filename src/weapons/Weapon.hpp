#pragma once
#include <iostream>
#include <list>

class Weapon
{
private:
    int power;

public:
    Weapon(int power);
    ~Weapon();

    //Accessors
    const int getPower() const {return this-> power;}

    //Setters
    void setPower(int levels);

    //Functions
    void upgrade(int levels);
};