#pragma once
#include "Character.hpp"

class Rogue : public Character
{
  public:

	Rogue(int body, int speed, int knowledge, int level ):Character(level){};
    void levelUp(int new_level);

    void set_level(int level);
    void equipWeapon(Dagger dagger);
    int attack();

};