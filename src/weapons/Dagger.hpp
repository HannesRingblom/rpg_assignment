#pragma once
#include "Weapon.hpp"

class Dagger : public Weapon
{
  private:
    int melee_damage;
    int ranged_damage;

  public:

	Dagger(int power ):Weapon(power){};
    void upgrade();
    int getTotalPower();
    int getMeleeDamage();
    int getRangedDamage();
};
