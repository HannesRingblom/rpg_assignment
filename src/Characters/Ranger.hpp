#pragma once
#include "Character.hpp"

class Ranger : public Character
{
  public:

	Ranger(int body, int speed, int knowledge, int level ):Character(level){};
    void levelUp(int new_level);

    void set_level(int level);
    void equipWeapon(Bow bow);
    void setRangedDamage(int dmg);
    int attack();
    int getDamage();

};