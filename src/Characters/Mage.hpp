#pragma once
#include "Character.hpp"

class Mage : public Character
{
  public:

	Mage(int body, int speed, int knowledge, int level ):Character(level){};
    void levelUp(int new_level);

    void set_level(int level);
    void equipWeapon(Staff staff);
    int attack();

};