#include "Sword.hpp"

Sword::Sword(int start_power):Weapon(start_power){}

void Sword::upgrade(){
    this->melee_damage += 5;
}
int Sword::getMeleeDamage(){
    return this->melee_damage;
}
int Sword::getTotalPower(){
    int power;
    power += this->melee_damage + this->getPower();
    return power;
}